<!-- This is needed to connect to the database. -->
<%@ include file="../WEB-INF/connect.jsp" %>

<%-- Set the query variable. --%>
<c:set var="query" value="SELECT * FROM ehsdata.timeline_data ORDER BY id DESC;"/>

<!-- If the query param has been set. -->
<c:if test="${not empty query}">
	<%-- Query the database with the specified query variable. --%>
	<sql:query var="qry" dataSource="${dataSource}">
		<%-- Print out the result. --%>
		<c:out value="${query}"/>
	</sql:query>
	
	<%-- Set the rowId variable to 0 -- the starting position. --%>
	<c:set var="rowId" value="0"/>
	
	<%-- Set up the output divider for the query result. It will be outputed in the JSON format. --%>
	<div id="output">
		[
		<c:forEach var="row" items="${qry.rowsByIndex}">
			{
			<c:forEach begin="0" end="${fn:length(row) - 1}" varStatus="loop">
				<c:set var="col" scope="page" value="${qry.columnNames}"/>
				"${col[loop.index]}" : "${row[loop.index]}"
				<c:if test="${loop.index lt fn:length(row) - 1}">, </c:if>
			</c:forEach>
			}
			
			<c:if test="${rowId lt qry.rowCount - 1}">, </c:if>
			
			<c:set var="rowId" value="${rowId + 1}"/>
		</c:forEach>
		]
	</div>
</c:if>

<%
	// Set the output attribute to the result returned by the query.
	request.setAttribute("output", pageContext.getAttribute("qry"));
%>