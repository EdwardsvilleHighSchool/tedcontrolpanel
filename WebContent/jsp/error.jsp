<!-- Tell the server that this is an error redirection page. -->
<%@page isErrorPage="true" %>

<!-- Page used for outputing error information to the user. -->
<html>
	<body>
<%
		// Get the status of the error.
		int    status  = response.getStatus();
		
		// Create a message describing the error. Default is an unknown error.
		String message = "Unknown error.";
		
		// If the page loaded correctly, say so.
		if (status == 200)
		{
			message = "Page successfully loaded... not an error.";
		}
		// If the error was caused by a missing login.
		else if (status == 401)
		{
			message = "Missing login.";
		}
		// If the error was caused by a forbidden directory listing.
		else if (status == 403)
		{
			message = "Forbidden directory listing.";
		}
		// If the error was caused by a page not being found.
		else if (status == 404)
		{
			message = "Page not found.";
		}
		// If the error was caused by an uncaught exception.
		else if (status == 500)
		{
			message = "Uncaught exception.";
			//message = exception.getMessage();
		}
		// If the error was caused by an unsupported servlet method.
		else if (status == 503)
		{
			message = "Unsupported servlet method.";
		}
		// If the error was caused by a gateway timeout.
		else if (status == 504)
		{
			message = "Gateway timeout.";
		}
%>
		<!-- Display a message describing the error. -->
		<p>Error <%= status %>: <%= message %></p>
	</body>
</html>