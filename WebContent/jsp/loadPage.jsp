<%
	// The URL to load.
	String url = request.getAttribute("url").toString();
%>

<script type="text/javascript" src="./js/functions.js"></script>
<script>
	// Set the content-panel's page to the specified URL.
	openPage("<%= url %>");
</script>