var postEntryLink;

var registerLink;

var loginLink;

var logoutLink;

var adminPanelLink;

function updateLinks()
{
	var rank = getRank();
	
	if (rank > 0)
	{
		loginLink.hide();
		logoutLink.show();
		postEntryLink.show();
	}
	else
	{
		loginLink.show();
		logoutLink.hide();
		postEntryLink.hide();
	}
	
	if (rank > 1)
	{
		registerLink.show();
	}
	else
	{
		registerLink.hide();
	}
	
	if (rank > 2)
	{
		adminPanelLink.show();
	}
	else
	{
		adminPanelLink.hide();
	}
}

function controlPanelReady()
{
	postEntryLink  = $("#post-entry-link");
    registerLink   = $("#register-link");
    loginLink      = $("#login-link");
    logoutLink     = $("#logout-link");
    adminPanelLink = $("#admincpanel-link");
    
    postEntryLink.hide();
    registerLink.hide();
    loginLink.hide();
    logoutLink.hide();
    adminPanelLink.hide();
    
    updateLinks();
}