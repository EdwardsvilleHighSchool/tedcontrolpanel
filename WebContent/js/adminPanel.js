var entries;

var numEntries = 0;

var includeBackButton = true;

/**
 * Callback function for the retrieveTimelineEvents function call.
 * 
 * @param id The id of the entry.
 * @param author The author that posted the entry.
 * @param title The title of the entry.
 * @param description The description of the entry.
 * @param url The URL to the entry's web-page.
 * @param date The date that the entry was posted.
 * @param time The time that the entry was posted.
 */
function entryReceived(id, author, title, description, url, date, time)
{
	entries.append(
			"<tr>" +
				"<td>" + author + "</td>" +
				"<td>" + title + "</td>" +
				"<td>" + description + "</td>" +
				"<td>" + date + "</td>" +
				"<td>" +
					"<input type='checkbox' id='" + id + "' name='" + id + "' value='" + id + "'>" +
				"</td>" +
			"</tr>");
	
	numEntries++;
}

function deleteEntries()
{
	$("input:checkbox").each(
		function()
		{
			if (this.checked)
			{
				var selector = $(this);
				
				var id = this.name;
				
				requestPage("deleteEntry", "id=" + id, "POST",
				        function(data, textStatus, jqXHR)
				        {
							var tr = selector.closest("tr");
							
							tr.remove();
							
							numEntries--;
							
							if (numEntries == 0)
							{
								entries.closest("form").remove();
							}
				        },
				        
				        function(XMLHttpRequest, textStatus, errorThrown)
				        {
				            alert("Failure!! Contact server administrator for assistance.");
				        }
				);
			}
		}
	);
}

function selectAll()
{
	$("#entries input:checkbox").prop('checked', true);
}

function adminPanelReady()
{
	var rank = getRank();
	
	if (rank < 3)
	{
		$(document.body).append("<p>You are not authorized to view this page.</p>");
		
		$("form").remove();
		
		return;
	}
	
	entries = $("#entries tbody");
	
	retrieveTimelineEvents(entryReceived);
	
	$("#select-all").click(selectAll);
	
	if (numEntries == 0)
	{
		entries.closest("form").remove();
		$(document.body).append("<p>There are currently no entries.</p>");
	}
}