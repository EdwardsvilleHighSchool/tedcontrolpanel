var errorElement;
var roleElement;

includeBackButton = true;

function checkRank()
{
	var rank = getRank();
	
	// If the user is at least a teacher.
	if (rank >= 2)
	{
		roleElement.style.display = "block";
		
		if (rank <= 2)
		{
			$('#role-select [value="teacher"]').remove();
		}
	}
}

function outputError(error)
{
	errorElement.innerHTML     = "Error: " + error;
	errorElement.style.display = "block";
}

function ready()
{
	$("#register-form").submit(
			function()
			{
				var username = $("#username").val();
				var password = $("#password").val();
				var confirm  = $("#confirm-password").val();
				
				if (username.length <= 0 || password.length <= 0 || confirm.length <= 0)
				{
					outputError("Please fill out all of the given fields.");
					
					return false;
				}
				else if (password != confirm)
				{
					outputError("Passwords did not match.");
					
					return false;
				}
		    	else if (username.length() > 25)
		    	{
		    		outputError("Name is too long.");
		    		
		    		return;
		    	}
		    	else if (password.length() > 1024)
		    	{
		    		outputError("Password is too long.");
		    		
		    		return;
		    	}
				
				return true;
			}
	);
	
	errorElement = document.getElementById("error");
	roleElement  = document.getElementById("role-select");
	errorElement.style.display = "none";
	roleElement.style.display  = "none";
	
	var error     = undefined;
	var arguments = document.location.hash.substring(1);
	
	eval(arguments);
	
	if (error != undefined)
	{
		outputError(error);
	}
	
	checkRank();
}