var errorElement;

includeBackButton = true;

function outputError(error)
{
	errorElement.innerHTML     = "Error: " + error;
	errorElement.style.display = "block";
}

function ready()
{
	$("#login-form").submit(
			function()
			{
				var username = $("#username").val();
				var password = $("#password").val();
				
				if (username.length <= 0 || password.length <= 0)
				{
					outputError("Please fill out all of the given fields.");
					
					return false;
				}
				
				return true;
			}
	);
	
	errorElement = document.getElementById("error");
	
	errorElement.style.display = "none";
	
	var error     = undefined;
	
	var arguments = document.location.hash.substring(1);
	
	eval(arguments);
	
	if (error != undefined)
	{
		outputError(error);
	}
}