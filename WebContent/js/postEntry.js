/**
 * This variable keeps track of which method of uploading is currently
 * being used by the user. The two options include: "Text" and "Upload"
 */
var uploadMethod;

var includeBackButton = true;

/**
 * Clear the current status of the upload. For instance: If
 * there was an error uploading the file and a message was showing
 * "An error has occurred," then remove the visibility of it.
 */
function clearStatus()
{
	$("#error").hide();
	$("#upload-status").hide();
}

function outputError(error)
{
	$("#error").html(error);
	$("#error").show();
}

/**
 * Change the form to the specified method. The possible
 * methods include:
 * <ul>
 * 	<li>Text</li>
 * 	<li>Upload</li>
 * </ul>
 * The <b>Text</b> method allows the user to post a single
 * html text snippet into its own page.
 * <br>
 * The <b>Upload</b> method allows the user to upload several
 * files to the server. Only one main page is required, the
 * rest is optional.
 * 
 * @param method The method in which to change to form to. Options
 * 		include: "<b>text</b>" and "<b>upload</b>" -- The case must
 * 		be lower case.
 */
function changeForm(method)
{
	$("#radio-buttons").css({ "display" : "block" });
	
	method = method.toLowerCase();
	
	uploadMethod = method;
	
	if (method == "text")
	{
		if ($("#inline-text").css("display") == "block")
		{
			return;
		}
		
		$("#page-upload").css({ "display" : "none" });
		$("#inline-text").css({ "display" : "block" });
		$("#upload-radio").prop("checked", "false");
		$("#text-radio").prop("checked", "true");
	}
	else if (method == "upload")
	{
		if ($("#page-upload").css("display") == "block")
		{
			return;
		}
		
		$("#page-upload").css({ "display" : "block" });
		$("#inline-text").hide();
		$("#text-radio").prop("checked", "false");
		$("#upload-radio").prop("checked", "true");
	}
	else
	{
		$("#radio-buttons").hide();
		$("#page-upload").hide();
		$("#inline-text").hide();
		$("#upload-status").hide();
		$("#error").hide();
	}
	
	clearStatus();
}

/**
 * Post the current entry that has been created. This takes
 * the values from the current page and uploads them to the
 * server.
 */
function postEntry()
{
	var title       = $("#project-title").val();
	var description = $("#description").val();
	var text        = undefined;
	
	if ($("#text") != undefined)
	{
		text = $("#text").val();
	}
	
	if (title.length > 0 && text.length > 0)
	{
		var urlData = "project-title=" + title;
		
		if (text != undefined)
		{
			urlData += "&text=" + text;
		}
		
		description = description.replace("&", "&amp;");
		
		urlData += "&description=" + description;
		
		requestPage("./addEntry", urlData, "POST",
				function(data, textStatus, jqXHR)
				{
					changeForm('none');
					
					console.log(data);
					
					var event = $.parseJSON(data);

					if (event.error == undefined)
					{
						$("#upload-status").css({ "display" : "block" });
						$("#upload-status").html("Successfully posted the entry!");
						
						event.on = new Date(event.on);
					}
					else
					{
						$("#error").css({ "display" : "block" });
						$("#error").html(event.error);
					}
				},
				function(XMLHttpRequest, textStatus, errorThrown)
				{
					alert("Failure!! Contact server administrator for assistance.");
					//alert(XMLHttpRequest.responseText);
					$("#error").html(XMLHttpRequest.responseText);
					$("#error").css({ "display" : "block" });
				}
		);
	}
	else
	{
		$("#error").css({ "display" : "block" });
		$("#error").html("You must fill out all of the fields!");
	}
}

function getTextUploadTitle()
{
	if (uploadMethod != "text")
	{
		return null;
	}
	
	var value = $("#project-title.text").val();
	
	return value;
}

function getFileUploadTitle()
{
	if (uploadMethod != "upload")
	{
		return null;
	}
	
	var value = $("#project-title.file").val();
	
	return value;
}

function getTitle()
{
	var title = getTextUploadTitle();
	
	if (title != null)
	{
		return title;
	}
	
	return getFileUploadTitle();
}

/**
 * The method that is called after the submit button has been selected.
 * Disables the button if necessary (to prevent double posting) and
 * checks for invalid inputs in the forms.
 */
function postSubmit()
{
	var title = getTitle();
	
	if (title.length <= 0)
	{
		outputError("Please fill out the title field.");
		
		return false;
	}
	if (uploadMethod == "text")
	{
		if ($("#text").val().length <= 0)
		{
			outputError("Please fill out the HTML field.");
			
			return false;
		}
	}
	
	var buttons = $(".submit-button");
	
	for (var i = 0; i < buttons.length; i++)
	{
		var button = $(buttons[i]);
		
		button.disabled = true;
		button.attr("disabled", "disabled");
		button.prop("disabled", true);
		button.addClass("ui-state-disabled");
	}
	
	return true;
}

/**
 * Set up all of the html when the document is ready for
 * showing.
 */
$(document).ready(function()
{
	var loggedIn = isLoggedIn();
	
	if (!loggedIn)
	{
		document.body.innerHTML = "<p>You must be <a href='login.html'>logged in</a> to access this page.</p>";
		
		return;
	}
	
	changeForm('none');
	
	var error     = null;
	var uploaded  = false;
	
	var arguments = document.location.hash.substring(1);
	
	eval(arguments);
	
	if (uploaded || error != null)
	{
		if (error != null)
		{
			$("#error").css({ "display" : "block" });
			$("#error").html("3RR0R: " + error);
		}
		else
		{
			$("#upload-status").css({ "display" : "block" });
			$("#upload-status").html("Successfully uploaded the files!");
		}
	}
	else
	{
		changeForm('text');
		
		var forms = $(".form");
		
		for (var i = 0; i < forms.length; i++)
		{
			var form = $(forms[i]);
			
			form.submit(function()
			{
				return postSubmit();
			});
		}
	}
});
