DROP DATABASE IF EXISTS authority;
CREATE DATABASE authority;
USE authority;

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `user_name` varchar(25) NOT NULL,
  `role_name` varchar(128) NOT NULL,
  PRIMARY KEY (`user_name`,`role_name`)
);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_name` varchar(25) NOT NULL,
  `user_pass` varchar(1024) NOT NULL,
  PRIMARY KEY (`user_name`)
);DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `user_name` varchar(25) NOT NULL,
  `role_name` varchar(128) NOT NULL,
  PRIMARY KEY (`user_name`,`role_name`)
);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_name` varchar(25) NOT NULL,
  `user_pass` varchar(1024) NOT NULL,
  PRIMARY KEY (`user_name`)
);

DROP DATABASE IF EXISTS ehsdata;
CREATE DATABASE ehsdata;
USE ehsdata;

DROP TABLE IF EXISTS `timeline_data`;

CREATE TABLE `timeline_data` (
  `id` int(11) NOT NULL,
  `title` varchar(400) NOT NULL,
  `date` varchar(100) NOT NULL,
  `time` varchar(100) NOT NULL,
  `url` varchar(400) NOT NULL,
  `author` varchar(200) NOT NULL,
  `description` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`)
);