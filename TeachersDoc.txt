This documentation file is intended for teachers only.
A teacher can assume the position of a student as well.

Student Entry Modification:
-----------------------
For all of the following actions, you will need to
traverse to the teachers panel.

 * Approval:
   1. Select the entry you would like to approve.
   2. Select the approve button.
 * Disproval:
   1. Select the entry you would like to disprove.
   2. Select the disprove button.
 * Modification:
   1. Select the entry you would like to modify.
   2. Select the modify button.
   3. After the modification is made, select the "confirm" button.