package net.interactivedisplayboard;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

// The URL in which the servlet is located at. e.g. http://localhost/addEntry
@WebServlet(urlPatterns = { "/logout" })
/**
 * Class used to log in a user if the credentials
 * are entered in correctly.
 * 
 * Date: 2/24/2014 6:00:00 PM
 */
public class LogoutUser extends EHSServlet
{
    @Override
    /**
     * Method called whenever a get request has been made on the
     * upload servlet.
     * 
     * @param request The request information from the sender.
     * @param response The response information to send back.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	doPost(request, response);
    }

    @Override
    /**
     * Method called whenever a post request has been made on the
     * retrieve entry servlet.
     * 
     * @param request The request information from the sender.
     * @param response The response information to send back.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	HttpSession session = request.getSession();
    	
    	if (session != null)
    	{
    		session.invalidate();
    		
    		forwardSuccess(request, response, "postLogout.html");
    	}
    	else
    	{
    		ServletOutputStream out = response.getOutputStream();
    	
    		out.println("You are not logged in.");
    	}
    }
}
