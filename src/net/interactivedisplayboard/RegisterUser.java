package net.interactivedisplayboard;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.interactivedisplayboard.database.DBConnection;
import net.interactivedisplayboard.logger.Log;
import net.interactivedisplayboard.logger.Logger;
import net.interactivedisplayboard.util.UserUtils;

// The URL in which the servlet is located at. e.g. http://localhost/addEntry
@WebServlet(urlPatterns = { "/registerUser" })
/**
 * Class used to register a user by adding them to
 * the database, if they do not already exist.
 * 
 * Date: 2/24/2014 6:00:00 PM
 */
public class RegisterUser extends EHSServlet
{
    @Override
    /**
     * Method called whenever a get request has been made on the
     * upload servlet.
     * 
     * @param request The request information from the sender.
     * @param response The response information to send back.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.getRequestDispatcher("/error404").forward(request, response);
    }

    @Override
    /**
     * Method called whenever a post request has been made on the
     * retrieve entry servlet.
     * 
     * @param request The request information from the sender.
     * @param response The response information to send back.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	String username = request.getParameter("username");
    	String password = request.getParameter("password");
    	String confirm  = request.getParameter("confirm-password");
    	String role     = request.getParameter("role-select");
    	
    	if (role == null)
    	{
    		role = "student";
    	}
    	
    	if (username.length() <= 0 || password.length() <= 0 || confirm.length() <= 0)
    	{
    		forwardError(request, response, "register.html", "Please fill out all of the fields.");
    		
    		return;
    	}
    	else if (!password.equals(confirm))
    	{
    		forwardError(request, response, "register.html", "Passwords did not match.");
    		
    		return;
    	}
    	else if (username.length() > 25)
    	{
    		forwardError(request, response, "register.html", "Name is too long.");
    		
    		return;
    	}
    	else if (password.length() > 1024)
    	{
    		forwardError(request, response, "register.html", "Password is too long.");
    		
    		return;
    	}
    	
    	password = md5(password);
    	
    	HttpSession session  = request.getSession();
    	
    	String authorityName = null;
    	
    	if (session != null)
    	{
    		authorityName = (String)session.getAttribute("username");
    	}
    	
    	if (!UserUtils.registerUser(username, password, role, authorityName))
    	{
    		forwardError(request, response, "register.html", authorityName + " does not have the authority to register a user of type " + role + ".");
    		
    		return;
    	}
    	else
    	{
    		forwardSuccess(request, response, "login.html");
    		
    		return;
    	}
    }
    
    /**
     * Encrypt the given String using the MD5 encryption algorithm.
     * 
     * @param input The String to encrypt.
     * @return The encrypted String.
     */
    private static String md5(String input)
    {
		String md5 = null;
		 
		if (null == input)
		{
			return null;
		}
		 
		try
		{   
			//Create MessageDigest object for MD5
			MessageDigest digest = MessageDigest.getInstance("MD5");
			 
			//Update input string in message digest
			digest.update(input.getBytes(), 0, input.length());
			
			//Converts message digest value in base 16 (hex) 
			md5 = new BigInteger(1, digest.digest()).toString(16);
			
		}
		catch (NoSuchAlgorithmException e)
		{
			throw new RuntimeException(e);
		}
		
		return md5;
	}
}
