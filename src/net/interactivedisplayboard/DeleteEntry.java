package net.interactivedisplayboard;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.interactivedisplayboard.database.DBConnection;
import net.interactivedisplayboard.logger.Log;
import net.interactivedisplayboard.logger.Logger;
import net.interactivedisplayboard.util.UserUtils;

// The URL in which the servlet is located at. e.g. http://localhost/addEntry
@WebServlet(urlPatterns = { "/deleteEntry" })
/**
 * Class used to delete a timeline entry from the database.
 * 
 * Date: 2/24/2014 6:00:00 PM
 */
public class DeleteEntry extends EHSServlet
{
    @Override
    /**
     * Method called whenever a get request has been made on the
     * upload servlet.
     * 
     * @param request The request information from the sender.
     * @param response The response information to send back.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.getRequestDispatcher("/error404").forward(request, response);
    }

    @Override
    /**
     * Method called whenever a post request has been made on the
     * retrieve entry servlet.<br>
     * <br>
     * Deletes the entry with the given id, if the user has the
     * authority to do so.
     * 
     * @param request The request information from the sender.
     * @param response The response information to send back.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	boolean loggedIn = UserUtils.isLoggedIn(request);
    	
    	if (loggedIn)
    	{
	    	int rank = UserUtils.getCurrentRank(request);
	    	
	    	if (rank >= UserUtils.ADMIN)
	    	{
	    		int entryId = Integer.parseInt(request.getParameter("id"));
	    		
	    		deleteEntry(entryId);
	    	}
    	}
    }
    
    private void deleteEntry(int id)
    {
    	DBConnection connection = DBConnection.getEhsDataConnection();
    	
    	try
    	{
			connection.query("DELETE FROM timeline_data WHERE id=" + id);
		}
    	catch (SQLException e)
    	{
			throw new RuntimeException(e);
		}
    }
}