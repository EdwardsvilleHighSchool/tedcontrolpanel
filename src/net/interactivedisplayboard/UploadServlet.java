package net.interactivedisplayboard;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.interactivedisplayboard.logger.Log;
import net.interactivedisplayboard.logger.Logger;
import net.interactivedisplayboard.multipart.MultipartMap;
import net.interactivedisplayboard.util.FilenameUtils;
import net.interactivedisplayboard.util.Location;

@WebServlet(urlPatterns = { "/upload" })
@MultipartConfig(location = "/home/ehs/upload/", maxFileSize = (10 * 1024 * 1024)) // 10MB.

/**
 * Class used to upload data to the server.
 * 
 * Date: 2/24/2014 5:00:00 PM
 */
public class UploadServlet extends EHSServlet
{
	private static final String	ALLOWED_FILE_TYPES[];
	private static final String	FORBIDDEN_FILE_TYPES[];
	
	/**
	 * Initialize the values for the ALLOWED and FORBIDDEN file type
	 * constants.
	 */
	static
	{
		ALLOWED_FILE_TYPES = new String[]
		{
			"html",
			"htm",
			"pdf",
			"jpg",
			"jpeg",
			"png",
			"gif",
			"js",
			"txt"
		};
		
		FORBIDDEN_FILE_TYPES = new String[]
		{
			"jsp"
		};
	}
	
    /**
     * Method called whenever a get request has been made on the
     * upload servlet.
     * 
     * @param request The request information from the sender.
     * @param response The response information to send back.
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.getRequestDispatcher("/error404").forward(request, response);
    }

    /**
     * Method called whenever a post request has been made on the
     * upload servlet.<br>
     * <br>
     * Uploads the data that is within the request parameter.
     * 
     * @param request The request information from the sender.
     * @param response The response information to send back.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	String ip          = getIP(request);
    	String title       = request.getParameter("project-title");
    	String description = request.getParameter("description");
    	
    	if (title.length() <= 0)
    	{
    		forwardError(request, response, "postEntry.html#uploaded=false;", "You must enter a title.");
    		
    		return;
    	}
    	
		Logger.log(Log.MESSAGE, ip, "About to start file upload.");
    	
		MultipartMap map = new MultipartMap(request, this);
		File mainPage[]  = new File[0];
		File files[]     = new File[0];
		
		if (map.containsData("main-page"))
		{
			mainPage = map.getFiles("main-page");
		}
		if (map.containsData("resource-files"))
		{
			files = map.getFiles("resource-files");
		}
		
		int    id   = 1;
		File   path = null;
		String url  = "postEntry.html";

		Logger.log(Log.MESSAGE, ip, "Querying the database for max id...");
		
		try
		{
			id = getMax("id", "timeline_data") + 1;
		}
		catch (SQLException e)
		{
			url         += "#uploaded=false;";
			
			String error = "Could not connect to the database. " + e.getMessage();
			
			forwardError(request, response, url, error);
			
			return;
		}
		
		Logger.log(Log.MESSAGE, ip, "Max ID value: " + id);
		
		String workingDirectory = Location.getInteractiveDisplayBoardContentRoot();
		String outputDirectory  = "/entries/" + id + "/";
		
		path = new File(workingDirectory + outputDirectory);
		
		Logger.log(Log.MESSAGE, ip, "Uploading data to the location: " + path.getAbsolutePath());
		
    	while (path.isDirectory())
		{
    		id++;
    		
    		outputDirectory = "/entries/" + id + "/";
    		path            = new File(workingDirectory + outputDirectory);
    		
			Logger.log(Log.ERROR, ip, "Directory for id " + id + " is already taken.");
		}
		
		if (mainPage == null || mainPage.length <= 0)
		{
			String error;
			
			url  += "#uploaded=false;";
			error = "No main-page was given to be uploaded.";
			
			forwardError(request, response, url, error);
			
			return;
		}
		
		path.mkdirs();
		
		String mainPageName = null;
		
		Logger.log(Log.MESSAGE, ip, "Uploading bunch of files...");
		
		// Start at -1 so we can also upload the main file.
		for (int i = -1; i < files.length; i++)
		{
			File file = null;
			
			if (i == -1)
			{
				file = mainPage[0];
			}
			else
			{
				file = files[i];
			}
			
			String name = uploadFile(file, path, ip);
			
			if (name == null)
			{
				String ext, error;
				
				url  += "#uploaded=false;";
				ext   = FilenameUtils.getExtension(file.getName());
				
				if (!isValidExtension(ext))
				{
					error = "Cannot upload file type \"" + ext + "\" to the server. (Not allowed)";
				}
				else
				{
					error = "File: \"" + file.getAbsolutePath() + "\" failed to upload.";
				}
				
				if (!deleteRecursively(path))
				{
					Logger.log(Log.ERROR, ip, "Unsuccessfully deleted \"" + path.getAbsolutePath() + '"');
				}
				
				forwardError(request, response, url, error);
				
				return;
			}
			
			if (i == -1)
			{
				mainPageName = name;
			}
		}

		Logger.log(Log.MESSAGE, ip, "Successfully uploaded bunch of files.");
		
		request.setAttribute("main-page", mainPageName);

		Logger.log(Log.MESSAGE, ip, "Adding the entry to the database...");
		AddEntryServlet.addEntryUpload(request, response, mainPageName, title, description);
		Logger.log(Log.MESSAGE, ip, "Successfully added the entry to the database.");
		
		url += "#uploaded=true;";
		
		Logger.log(Log.MESSAGE, ip, "Forwarding page to: " + url);
		forwardSuccess(request, response, url);
    }
    
    /**
     * Upload the specified File to the specified path.
     * 
     * @param file The File to upload to the specified path.
     * @param path The location in which to upload the file to.
     * @param ip The IP in which the File was uploaded from.
     * @return The name of the file that was uploaded.
     */
    private static String uploadFile(File file, File path, String ip)
    {
    	String tempName = file.getName();
    	
    	Logger.log(Log.MESSAGE, ip, "Uploading temp file \"" + tempName + "\" to the server.");
    	
    	String extension = FilenameUtils.getExtension(file.getName());
    	
    	if (!isValidExtension(extension))
    	{
        	Logger.log(Log.ERROR, ip, "Cannot upload file type \"" + extension + "\" to the server. (Not allowed)");
        	
        	return null;
    	}
    	
    	if (extension.length() > 0)
    	{
    		extension = '.' + extension;
    	}
    	
    	String name = file.getName().substring(0, file.getName().lastIndexOf('_')) + extension;
    	
        File outputFile = new File(path, name);
    	
        file.renameTo(outputFile);
        
        if (outputFile.exists()) 
        {
        	Logger.log(Log.MESSAGE, ip, "File \"" + name + "\" uploaded successfully to \"" + path + "\"");
        	
        	return name;
        }
        else
        {
        	Logger.log(Log.ERROR, ip, "File \"" + name + "\" failed to upload.");
        	
        	return null;
        }
    }
    
    /**
     * Delete files recursively, if the file is a directory. If not
     * then just delete the file.
     * 
     * @param file The file/directory to delete recursively.
     * @return Whether or not the deletion was successful.
     */
    private static boolean deleteRecursively(File file)
    {
    	if (!file.isDirectory())
    	{
    		return file.delete();
    	}
    	
    	File children[] = file.listFiles();
    	
    	for (int i = 0; i < children.length; i++)
    	{
    		File child = children[i];
    		
    		if (!deleteRecursively(child))
    		{
    			return false;
    		}
    	}
    	
    	return file.delete();
    }
    
    /**
     * Get whether or not the given extension is an okay file type to
     * upload to the server.
     * 
     * @param extension The extension (aka file type)
     * @return Whether or not the given extension is okay to upload.
     */
    private static boolean isValidExtension(String extension)
    {
    	for (int i = 0; i < FORBIDDEN_FILE_TYPES.length; i++)
    	{
    		String forbidden = FORBIDDEN_FILE_TYPES[i];
    		
    		if (extension.equals(forbidden))
    		{
    			return false;
    		}
    	}
    	
    	for (int i = 0; i < ALLOWED_FILE_TYPES.length; i++)
    	{
    		String allowed = ALLOWED_FILE_TYPES[i];
    		
    		if (extension.equals(allowed))
    		{
    			return true;
    		}
    	}
    	
    	return false;
    }
}