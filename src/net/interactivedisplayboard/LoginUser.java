package net.interactivedisplayboard;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.interactivedisplayboard.util.UserUtils;

// The URL in which the servlet is located at. e.g. http://localhost/addEntry
@WebServlet(urlPatterns = { "/loginUser" })
/**
 * Class used to log in a user if the credentials
 * are entered in correctly.
 * 
 * Date: 2/24/2014 6:00:00 PM
 */
public class LoginUser extends EHSServlet
{
    @Override
    /**
     * Method called whenever a get request has been made on the
     * upload servlet.
     * 
     * @param request The request information from the sender.
     * @param response The response information to send back.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.getRequestDispatcher("/error404").forward(request, response);
    }

    @Override
    /**
     * Method called whenever a post request has been made on the
     * retrieve entry servlet.
     * 
     * @param request The request information from the sender.
     * @param response The response information to send back.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	String username = request.getParameter("username");
    	String password = request.getParameter("password");
    	
    	password = md5(password);
    	
    	if (!UserUtils.validCredentials(username, password))
    	{
    		forwardError(request, response, "login.html", "Incorrect name or password.");
    		
    		return;
    	}
    	
    	username = UserUtils.getUsername(username);
    	
    	HttpSession session = request.getSession();
    	
    	if (session != null)
    	{
    		session.invalidate();
    	}
    	
    	session  = request.getSession(true);
    	
    	int rank = UserUtils.getHighestRank(username);
    	
    	session.setAttribute("username", username);
    	session.setAttribute("rank", rank);
    	
    	forwardSuccess(request, response, "postLogin.html");
    }
    
    private static String md5(String input)
    {
		String md5 = null;
		 
		if (null == input)
		{
			return null;
		}
		 
		try
		{   
			//Create MessageDigest object for MD5
			MessageDigest digest = MessageDigest.getInstance("MD5");
			 
			//Update input string in message digest
			digest.update(input.getBytes(), 0, input.length());
			
			//Converts message digest value in base 16 (hex) 
			md5 = new BigInteger(1, digest.digest()).toString(16);
			
		}
		catch (NoSuchAlgorithmException e)
		{
			throw new RuntimeException(e);
		}
		
		return md5;
	}
}
