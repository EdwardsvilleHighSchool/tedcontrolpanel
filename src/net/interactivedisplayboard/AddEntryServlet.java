package net.interactivedisplayboard;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.interactivedisplayboard.database.DBConnection;
import net.interactivedisplayboard.logger.Log;
import net.interactivedisplayboard.logger.Logger;
import net.interactivedisplayboard.util.Location;

// The URL in which the servlet is located at. e.g. http://localhost/addEntry
@WebServlet(urlPatterns = { "/addEntry" })
/**
 * Class used to add a timeline entry to the database.
 * 
 * Date: 2/24/2014 6:00:00 PM
 */
public class AddEntryServlet extends EHSServlet
{
    @Override
    /**
     * Method called whenever a get request has been made on the
     * upload servlet.
     * 
     * @param request The request information from the sender.
     * @param response The response information to send back.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.getRequestDispatcher("/error404").forward(request, response);
    }

    @Override
    /**
     * Method called whenever a post request has been made on the
     * upload servlet.<br>
     * <br>
     * Uploads the data that is within the request parameter.
     * 
     * @param request The request information from the sender.
     * @param response The response information to send back.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	String mainPage    = (String)request.getAttribute("main-page");
    	String title       = request.getParameter("project-title");
    	String text        = request.getParameter("text");
    	String description = request.getParameter("description");

    	ServletOutputStream out = response.getOutputStream();
    	
    	String output = addEntry(request, response, title, text, description);
    	
    	out.println(output);
    }
    
    /**
     * Add an entry to the default database for when files were uploaded.
     * 
     * @param request The Servlet request variable.
     * @param response The Servlet response variable.
     * @param mainPage The name of the main-page that is being uploaded.
     * @param title The title of the entry that is being added.
     * @return The output of the entry in JSON format (if it was a success)
     * @throws IOException Thrown if there were any IO errors.
     */
    public static String addEntryUpload(HttpServletRequest request, HttpServletResponse response, String mainPage, String title, String description) throws IOException
    {
    	return addEntry(request, response, mainPage, title, null, description);
    }
    
    /**
     * Add an entry to the default database for when an entry is being
     * added based on a basic inline-HTML single-page upload.
     * 
     * @param request The Servlet request variable.
     * @param response The Servlet response variable.
     * @param title The title of the entry that is being added.
     * @param text The HTML of the page that is being uploaded.
     * @return The output of the entry in JSON format (if it was a success)
     * @throws IOException Thrown if there were any IO errors.
     */
    public static String addEntry(HttpServletRequest request, HttpServletResponse response, String title, String text, String description) throws IOException
    {
    	return addEntry(request, response, null, title, text, description);
    }
    
    /**
     * Add an entry to the default database for when either:
     * <ul>
     * 	<li>Multiple files are being uploaded</li>
     * 	<li>A inline-HTML single-page is being uploaded</li>
     * </ul>
     * 
     * @param request The Servlet request variable.
     * @param response The Servlet response variable.
     * @param mainPage The name of the main-page that is being uploaded.
     * @param title The title of the entry that is being added.
     * @param text The HTML of the page that is being uploaded.
     * @return The output of the entry in JSON format (if it was a success)
     * @throws IOException Thrown if there were any IO errors.
     */
    public static String addEntry(HttpServletRequest request, HttpServletResponse response, String mainPage, String title, String text, String description) throws IOException
    {
    	HttpSession session = request.getSession();
    	
    	if (session == null)
    	{
    		String error = "You must be logged in to post an entry.";
    		
    		outputError(error);
    		
    		return null;
    	}
    	
    	String ip = getIP(request);
    	
    	if (mainPage == null)
    	{
    		mainPage = "index.html";
    	}
    	
    	String url, date, time, author;
    	
    	author = (String)session.getAttribute("username");
    	
    	if (title == null)// || (text == null && mainPage == null))
    	{
    		String error = "The required arguments were not supplied.";
    		
    		outputError(error);
    	}
    	
		// Set the default value to 1 if no entries are present.
		int id = 1;
		
		try
		{
			DBConnection connection;
			String workingDirectory, outputDirectory;
			
			connection = DBConnection.getEhsDataConnection();
			id         = getMax("id", "timeline_data") + 1;

			workingDirectory = Location.getInteractiveDisplayBoardContentRoot();
			outputDirectory  = "/entries/" + id + "/";
			
			File newDir = new File(workingDirectory + outputDirectory);
			
	    	while (newDir.isDirectory())
			{
	    		outputDirectory = "/entries/" + ++id + "/";
	    		
	    		newDir = new File(workingDirectory + outputDirectory);
			}
	    	
			SimpleDateFormat format;
			Date d;
			
			format = new SimpleDateFormat("yyyy-MM-dd");
			d      = new Date();
			date   = format.format(d);
			format = new SimpleDateFormat("HH:mm:ss");
			time   = format.format(d);
			url    = outputDirectory + mainPage;
			
			// If it was a text entry, not a file upload entry.
			if (text != null)
			{
				newDir.mkdirs();
				
				File newPage = new File(workingDirectory + url);
				newPage.createNewFile();
				
				// Create a file writer and write to the new web-page.
				FileWriter fw = new FileWriter(newPage);
				fw.write(text);
				fw.close();
			}
			else
			{
	    		outputDirectory = "/entries/" + --id + "/";
				url             = outputDirectory + mainPage;
			}
			
			return finishEntry(connection, id, title, date, time, url, author, description);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			
			Logger.log(Log.ERROR, ip, "Could not query database.");
		}
		
    	return null;
    }
    
    /**
     * Finish adding the entry to the database and output the data in the
     * output div.
     * 
     * @param connection The Database connection to enter the data into.
     * @param id The id of the entry to add.
     * @param title The title of the entry to add.
     * @param date The date the entry is being added.
     * @param time The time the entry is being added.
     * @param url The url the entry is located at.
     * @param author The author that posted the entry.
     * @param out The output stream used to output the div data.
     * @return The output of the entry in JSON format.
     * @throws IOException Thrown if there is an error outputting the div.
     * @throws SQLException Thrown if there is an error entering the entry.
     */
    private static String finishEntry(DBConnection connection, int id, String title, String date, String time, String url, String author, String description) throws IOException, SQLException
    {
    	title       = formatString(title);
    	url         = formatString(url);
    	author      = formatString(author);
    	description = formatString(description);
    	
    	String query = "INSERT INTO timeline_data VALUES(" + id + ", '" + title + "', '" + date + "', '" + time + "', '" + url + "', '" + author + "', '" + description + "');";
		connection.query(query);
		
		return "{ " +
					"\"id\": " + id + ", " +
					"\"title\": \"" + title + "\", " +
					"\"on\": \"" + date + "\", " +
					"\"url\": \"" + url + "\", " + 
					"\"author\": \"" + author + "\", " + 
					"\"description\": \"" + description + "\" " + 
				"}";
    }
}
