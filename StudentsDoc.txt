This documentation file is intended for students.

Post an Entry:
-----------------------
For all of the following actions, you will need to
traverse to the "Post entry" page.

 * HTML Text Submission:
   1. Make sure the "HTML Text" radio button is selected at the top of the page.
   2. Enter the title for your project in the provided space.
   3. Enter any HTML code for your project in the other provided space.
   4. Select the submit button.